package com.company;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Person implements Comparable<Person> {
    private String fio;
    private int age;

    @Override
    public int compareTo(Person o) {
//        return this.age.compareTo(o.age);
        return 0;
    }
}
