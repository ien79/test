package com.company;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class TestClass {

    @Test
    public static void Test01() {
        int a = 5;
        int b = 7;

        a += b;

    }

    public static void Test02(String[] arg) {
        if (arg.length > 0) {
            Integer[] arr = new Integer[arg.length];
            for (int i = 0; i < arg.length - 1; i++) {
                arr[i] = Integer.parseInt(arg[i]);
            }
            Arrays.sort(arr);
            for (int i = arr.length - 1; i != -1; i--) {
                System.out.println(arr[i]);

            }
        }
    }

    @Test
    public void Test03() {
        List<String> lines = null;

        try {
            lines = Files.readAllLines(Paths.get("d:\\test.txt"));
            String[] words;
            for (String str : lines) {
                words = str.split(" ");
                Person person = new Person(words[0] + words[1], Integer.parseInt(words[2]));


            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
